import * as express from 'express';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import * as moment from 'moment';
import { environment as env } from '../../../environment';
import { IAuthResponse } from '../types/IAuthResponse';
import { IUserParams } from '../types/IUserParams';
import { IUserProfile } from '../types/IUserProfile';
import { IUser } from '../types/IUser';
import { UsersController } from '../controllers/users.controller';
import { verifyJwt } from '../middlewares/verifyJwt';

const router = express.Router();
const userCtrl = new UsersController();

router.post('/register', async (req: express.Request, res: express.Response): Promise<void> => {
    req.checkBody('email', 'Email address is required.').notEmpty();
    req.checkBody('email', 'Enter a valid email address.').isEmail();
    req.checkBody('password', 'Password is required.').notEmpty();
    req.checkBody('password', 'Password must be at least 8 characters long.').isLength({ min: 8 });
    req.checkBody('passwordConfirmation', 'Confirmation password is required.').notEmpty();
    req.checkBody('passwordConfirmation', 'Passwords don\'t match.').equals(req.body.password);

    const errors = req.validationErrors();

    if (errors) {
        res.status(422).send(errors);
        return;
    } else {
        let account: IUser;
        try {
            const hashedPassword = await bcrypt.hash(req.body.password, 10);
            account = {
                email: req.body.email,
                password: hashedPassword
            };
            await userCtrl.create(account);
            res.json({ success: true, message: 'Account successfully created.' });
        } catch (err) {
            if (err.code === 11000) {
                res.status(409).json({ succes: false, message: 'This email address is already taken.' });
            } else {
                res.status(500).end();
            }
        }
    }
});


router.post('/auth', async (req: express.Request, res: express.Response): Promise<void> => {
    const failMsg = 'Authentication failed.';

    req.checkBody('email', 'Email address is required.').notEmpty();
    req.checkBody('email', 'Enter a valid email address.').isEmail();
    req.checkBody('password', 'Password must be at least 8 characters long.').isLength({ min: 8 });
    req.checkBody('password', 'Password is required.').notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        res.status(422).send(errors);
        return;
    } else {
        const params: IUserParams = {
            email: req.body.email
        };
        try {
            const user = await userCtrl.find(params);
            const userId = {
                _id: user._id
            };

            if (user) {
                const passwordIsOk = await bcrypt.compare(req.body.password, user.password);
                if (!passwordIsOk) {
                    res.status(403).json({ error: failMsg });
                } else {
                    const token = jwt.sign(userId, env.SECRET, { expiresIn: '8h' });
                    const expirationDate = moment().add(8, 'hours').format('YYYY-MM-DD HH:mm');
                    const authResp: IAuthResponse = {
                        accessToken: token,
                        expiresAt: expirationDate,
                        profile: {
                            _id: user._id,
                            email: user.email
                        }
                    };
                    res.json(authResp);
                }
            } else {
                res.status(404).json({ error: failMsg });
            }
        } catch (err) {
            res.status(403).json({ error: failMsg });
        }
    }
});

router.use('/users', verifyJwt);

router.get('/users/:id', async (req: express.Request, res: express.Response): Promise<void> => {
    const params: IUserParams = {
        _id: req.params.id
    };

    try {
        if (params._id === (<any>req).decoded._id) {
            const user = await userCtrl.find(params);
            user.password = undefined;
            res.json(user);
        } else {
            res.status(404).send({ error: 'Noting was found!' });
        }
    } catch (err) {
        res.status(500).end();
    }
});

router.put('/users/:id', async (req: express.Request, res: express.Response): Promise<void> => {
    req.checkBody('currentPassword', 'Password is required.').notEmpty();
    req.checkBody('password', 'Password is required.').notEmpty();
    req.checkBody('passwordConfirmation', 'Confirmation password is required.').notEmpty();
    req.checkBody('passwordConfirmation', 'Passwords don\'t match.').equals(req.body.password);

    const errors = req.validationErrors();

    if (errors) {
        res.status(422).send(errors);
        return;
    } else {
        const params: IUserParams = {
            _id: (<any>req).decoded._id
        };
        const user = await userCtrl.find(params);
        const currentPassIsOk = await bcrypt.compare(req.body.currentPassword, user.password);

        if (currentPassIsOk) {
            let updatedUser: Object = {};
            try {
                const hashedPassword = await bcrypt.hash(req.body.password, 10);
                updatedUser = { password: hashedPassword };
                await userCtrl.update(params, updatedUser);
                res.json({ message: 'Your password was successfully updated.' });
            } catch (err) {
                res.status(500).end();
            }
        } else {
            res.status(403).json({ error: 'Current password it\'s not valid.' });
        }
    }
});

module.exports = router;
