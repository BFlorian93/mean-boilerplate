import * as express from 'express';
import * as jwt from 'jsonwebtoken';
import { environment as env } from '../../../environment';

export async function verifyJwt(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
    // Check header for token.
    const token = req.headers['authorization'] as string;

    // Decode the token if it exists.
    if (token) {
        try {
            // Verfify secret and expiration date.
            const decoded = await jwt.verify(token, env.SECRET);
            // If everything is good, save decoded to request to use in other routes.
            (<any>req).decoded = decoded;
            next();
        } catch (err) {
            res.status(403).json({ error: 'Failed to authenticate token.' });
        }
    } else {
        // If there is no token return an error.
        res.status(403).send({ error: 'No token provided!' });
    }
}
