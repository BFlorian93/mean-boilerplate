import { Request, Response } from 'express';
import { UserModel as User, UserDocument } from '../models/user';
import { IUserParams } from '../types/IUserParams';


export class UsersController {
    /**
     * Class constructor.
     */
    constructor() { }

    /**
     * Find the user.
     *
     * @param  {IUserParams} theUser
     * @return {Promise}
     */
    async find(theUser: IUserParams): Promise<UserDocument | null> {
        const user = await User.findOne(theUser).exec();
        if (!user) {
            throw new Error('User not found!');
        }

        return user;
    }

    /**
     * Create a new user.
     *
     * @param  {Object}  newUser
     * @return {Promise}
     */
    async create(newUser: Object): Promise<void> {
        const account = new User(newUser);
        await account.save();
    }

    /**
     * Update user's details.
     *
     * @param  {IUserParams}  user
     * @param  {Object}       details
     * @return {Promise}
     */
    async update(user: IUserParams, details: Object): Promise<void> {
        await User.findOneAndUpdate(user, details).exec();
    }
}
