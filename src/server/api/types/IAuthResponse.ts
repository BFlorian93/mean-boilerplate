import { IUserProfile } from './IUserProfile';

export interface IAuthResponse {
    accessToken: string;
    expiresAt: string;
    profile: IUserProfile;
}
