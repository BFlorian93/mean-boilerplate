import * as express from 'express';
import * as mongoose from 'mongoose';
import * as bodyParser from 'body-parser';
import * as morgan from 'morgan';
import * as path from 'path';
import * as validator from 'express-validator';
import { environment as env } from './environment';

const dbUrl = env.DB_URI;

// If the connection is successfull.
mongoose.connection.on('connected', () => {
    console.log(`Connected to ${dbUrl}.`);

    const app = express();

    app.use(express.static(path.join(__dirname, '../node_modules')));
    app.use(express.static(path.join(__dirname, './client')));
    app.use(express.static(path.join(__dirname, '../')));

    app.use(validator());

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(morgan('dev'));

    app.use('/api', require('./server/api/routes/users.routes'));

    app.get('*', (req, res) => {
        res.sendFile(path.join(__dirname, './client/index.html'));
    });

    const port: string | number = env.PORT || 8000;

    app.listen(port, () => {
        console.log(`App running at localhost:${port}`);
    });
});

// If the connection throws an error.
mongoose.connection.on('error', (err) => {
    console.error(`Failed to connect to ${dbUrl} on startup.`, err);
});

// When the connection is disconnected.
mongoose.connection.on('disconnected', () => {
    console.log(`Mongoose default connection to ${dbUrl} is disconnected.`);
});

// If the Node process ends, close the Mongoose connection.
mongoose.connection.on('SIGINT', () => {
    mongoose.connection.close(() => {
        console.log(`Mongoose default connection to ${dbUrl} is disconnected due to app termination.`);
        process.exit(0);
    });
});

try {
    (mongoose as any).Promise = global.Promise;
    mongoose.connect(dbUrl);
    console.log(`Trying to connect to ${dbUrl}.`);
} catch (err) {
    console.log(`Server initialization failed ${err.message}`);
}
