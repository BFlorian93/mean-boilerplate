# Simple MEAN Boilerplate

## Requirements

* [Node and npm](https://nodejs.org/)
* MongoDB Database: [mLab](https://mlab.com/) or [local](https://www.mongodb.com/download-center)
* Optional [nodemon](https://nodemon.io/)

## Installation

* Download the [repository]()
* Install node modules: `npm install`
* Set the `DB_URI` and `SECRET` in environment.ts file
* Compile files: `npm run build`
* Start the server: `npm start` or, if you have nodemon installed: `npm run dev`

## Usage

* Visit the application in your browser at http://localhost:8000