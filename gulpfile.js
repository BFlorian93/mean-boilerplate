const gulp = require('gulp');
const ts = require('gulp-typescript');
const tslint = require('gulp-tslint');
const sourcemaps = require('gulp-sourcemaps');
const runSequence = require('run-sequence');
const tsProject = ts.createProject('./tsconfig.json');

gulp.task('compile:server', () => {
    return gulp.src(['src/**/*.ts', '!src/client/**/*.ts'])
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .pipe(sourcemaps.write('.', { includeContent: false }))
        .pipe(gulp.dest('./build'));
});

gulp.task('compile:client', () => {
    return gulp.src('src/client/**/*.ts')
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .pipe(sourcemaps.write('.', { includeContent: false, sourceRoot: '../../src' }))
        .pipe(gulp.dest('./build/client'));
});

gulp.task('lint', () => {
    return gulp.src(['src/**/*.ts', '!src/client/**/*.ts'])
        .pipe(tslint({
            formatter: 'verbose',
            configuration: 'tslint.json'
        }))
        .pipe(tslint.report({
            summarizeFailureOutput: true
        }));
});

gulp.task('lint:client', () => {
    return gulp.src('src/client/**/*.ts')
        .pipe(tslint({
            formatter: 'verbose',
            configuration: 'tslint.json'
        }))
        .pipe(tslint.report({
            summarizeFailureOutput: true
        }));
});

gulp.task('tslint', () => {
    runSequence('lint', 'lint:client');
});

gulp.task('static', () => {
    return gulp.src('./src/**/*.+(html|css|js)')
        .pipe(gulp.dest('./build'));
});

gulp.task('default', () => {
    runSequence('static', 'compile:server', 'compile:client');
});